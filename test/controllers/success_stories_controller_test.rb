require 'test_helper'

class SuccessStoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @success_story = success_stories(:one)
  end

  test "should get index" do
    get success_stories_url
    assert_response :success
  end

  test "should get new" do
    get new_success_story_url
    assert_response :success
  end

  test "should create success_story" do
    assert_difference('SuccessStory.count') do
      post success_stories_url, params: { success_story: { description: @success_story.description, owner: @success_story.owner, pet_name: @success_story.pet_name, photo: @success_story.photo } }
    end

    assert_redirected_to success_story_url(SuccessStory.last)
  end

  test "should show success_story" do
    get success_story_url(@success_story)
    assert_response :success
  end

  test "should get edit" do
    get edit_success_story_url(@success_story)
    assert_response :success
  end

  test "should update success_story" do
    patch success_story_url(@success_story), params: { success_story: { description: @success_story.description, owner: @success_story.owner, pet_name: @success_story.pet_name, photo: @success_story.photo } }
    assert_redirected_to success_story_url(@success_story)
  end

  test "should destroy success_story" do
    assert_difference('SuccessStory.count', -1) do
      delete success_story_url(@success_story)
    end

    assert_redirected_to success_stories_url
  end
end
