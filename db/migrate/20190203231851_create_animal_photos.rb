class CreateAnimalPhotos < ActiveRecord::Migration[5.2]
  def change
    create_table :animal_photos do |t|
      t.string :content

      t.timestamps
    end
  end
end
