# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

# Para adição de Foto
$(document).on 'turbolinks:load', ->
  $('#new_animal_photo').on("ajax:success", (event) ->
    [data, status, xhr] = event.detail
    answer = JSON.parse(xhr.response)
    $('#animal_photos').append '<div><img src="'+answer["content"]["url"]+'" /><p><a data-remote="true" rel="nofollow" data-method="delete" href="/fotos-animal/'+answer["id"]+'">Excluir</a></p></div>'

  ).on "ajax:error", (event, xhr) ->
    [data, status, xhr] = event.detail
    answers = JSON.parse(xhr.response)
    for answer in answers
      alert(answer)

# Na remoção da Foto
$(document).on 'turbolinks:load', ->
  $(document).on "ajax:success", '#animal_photos a', (event) -> # Necessário bindar ao document, pois assim o link de remoção ativa o ajax.
    linkDelete = $(event["target"])
    linkDelete.parent().parent().html("")
